FROM ubuntu:16.04

# Distribution configuration


### Add some core repos
RUN apt-get update && \
        apt-get install -y sudo curl zip openssl build-essential python-software-properties && \
        apt-get clean;

# This is in accordance to : https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-ubuntu-16-04
RUN apt-get update && \
        apt-get install -y openjdk-8-jdk && \
        apt-get install -y ant && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/* && \
        rm -rf /var/cache/oracle-jdk8-installer;

# Fix certificate issues, found as of
# https://bugs.launchpad.net/ubuntu/+source/ca-certificates-java/+bug/983302
RUN apt-get update && \
        apt-get install -y ca-certificates-java && \
        apt-get clean && \
        update-ca-certificates -f && \
        rm -rf /var/lib/apt/lists/* && \
        rm -rf /var/cache/oracle-jdk8-installer;

# Setup JAVA_HOME, this is useful for docker commandlin                                                              
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
RUN export JAVA_HOME

# Define environment variables.
ENV MULE_HOME=/opt/mule
ENV MULE_VERSION=4.3.0-ea2
ENV MULE_MD5=1c77132085645169b32647193e4ec265
ENV TZ=Asia/Kolkata
ENV MULE_USER=mule

# SSL Cert for downloading mule zip
RUN apt-get update
RUN apt-get upgrade
RUN apt-get install ca-certificates -y
RUN update-ca-certificates
RUN apt-get install openssl
RUN apt-get install tzdata
RUN rm -rf /var/cache/apk/*

RUN adduser ${MULE_USER}

RUN mkdir /opt/mule-standalone-${MULE_VERSION}
RUN ln -s /opt/mule-standalone-${MULE_VERSION} ${MULE_HOME}
RUN chown ${MULE_USER}:${MULE_USER} -R /opt/mule*


RUN echo ${TZ} > /etc/timezone

RUN apt-get install wget

USER ${MULE_USER}
# Checksum
RUN cd ~ && wget https://repository-master.mulesoft.org/nexus/content/repositories/releases/org/mule/distributions/mule-standalone/${MULE_VERSION}/mule-standalone-${MULE_VERSION}.tar.gz
#RUN echo "${MULE_MD5} mule-standalone-${MULE_VERSION}.tar.gz" | md5sum -c
WORKDIR /opt
RUN tar -xvzf ~/mule-standalone-${MULE_VERSION}.tar.gz
RUN rm ~/mule-standalone-${MULE_VERSION}.tar.gz

# Define mount points.
VOLUME ["${MULE_HOME}/logs", "${MULE_HOME}/conf", "${MULE_HOME}/apps", "${MULE_HOME}/domains"]

# Define working directory.
WORKDIR ${MULE_HOME}

USER root

CMD ["/opt/mule/bin/mule"]

# Default http port
EXPOSE 8081


